# ---
# jupyter:
#   jupytext:
#     formats: py:percent,ipynb
#     text_representation:
#       extension: .py
#       format_name: percent
#       format_version: '1.3'
#       jupytext_version: 1.14.7
#   kernelspec:
#     display_name: Python 3 (ipykernel)
#     language: python
#     name: python3
# ---
# %% [markdown]
# # Cold Start Forecasting Example
# The data we use in this example is [Visuelle](https://paperswithcode.com/dataset/visuelle):
# >VISUELLE is a repository build upon the data of a real fast fashion company, Nunalie, and is
# composed of 5577 new products and about 45M sales related to fashion seasons from 2016-2019.
# Each product in VISUELLE is equipped with multimodal information: its image, textual metadata,
# sales after the first release date, and three related Google Trends describing category, color
# and fabric popularity.
#
# To run the notebook, follow the link above to download the data and specify the path to the
# data in the notebook.
#
# In the following notebook we will compare the performance of different forecasting models:
# - GTM Model. It is a model that was presented in the paper ["Well googles is half done" by Skenderi et al.](https://arxiv.org/abs/2109.09824)
# - Simple statistical models
#   - Mean of all sales
#   - Mean sales of products that belong to the same category: as a constant and stratified by time step
# - XGBoost
# - CatBoost
#
# %% [markdown]
# ## Utils and Imports
# %%
from pathlib import Path
import xgboost as xgb
from sklearn.metrics import (
    mean_squared_error,
    mean_absolute_error,
    root_mean_squared_error,
)
import numpy as np
import pandas as pd
from IPython.core.getipython import get_ipython
from plotly.subplots import make_subplots

from catboost import CatBoostRegressor

import plotly.graph_objects as go
from plotly.subplots import make_subplots
import plotly.express as px
from pathlib import Path


# %% [markdown]
# ## Load & Prepare Data
# %%
# Keep in mind that "Well googled is half done" paper used visuelle
# dataset, not to be confused with visuelle2.0. In this notebook we
# will use the former.

# TODO: specify the path to the data
folder_path = Path("data") / "visuelle"

# Note: The normalization scale is specified in the Visuelle dataset.
# Here, we specify it manually for convenience and transparency.
norm_scale = 1065

train_raw = pd.read_csv(folder_path / "train.csv", parse_dates=["release_date"])
test_raw_unfiltered = pd.read_csv(
    folder_path / "test.csv", parse_dates=["release_date"]
)
# %%
train_raw.head()
# %%
train_raw.columns
# %% [markdown]
# Note that some items from the test set have `release_date` that also appear in the train set.
# To avoid data leakage, we will remove these items from the test set.
# %%
px.histogram(
    pd.concat(
        [
            train_raw[["release_date"]].assign(type="train"),
            test_raw_unfiltered[["release_date"]].assign(type="test"),
        ]
    ),
    x="release_date",
    color="type",
    barmode="overlay",
    title="Release date distribution",
)
# %%
test_raw = test_raw_unfiltered.query("release_date > @train_raw.release_date.max()")

print(f"Before filtering, test set had {len(test_raw_unfiltered)} items.")
print(f"After filtering, test set has {len(test_raw)} items.")
# %%
# The first 11 columns correspond to time steps
meta_columns = list(train_raw.columns[12:])

train = (
    train_raw.melt(id_vars=meta_columns, value_name="target", var_name="time_step")
    .assign(target=lambda x: (x.target * norm_scale).round(0).astype(int))
    .astype({"time_step": int})
)

test = (
    test_raw.melt(id_vars=meta_columns, value_name="target", var_name="time_step")
    .assign(target=lambda x: (x.target * norm_scale).round(0).astype(int))
    .astype({"time_step": int})
)

# %% [markdown]
# ## EDA
# %%
# Note: there are rows where target is negative. For simplicity we will not
# have a special treatment for such cases. Potentially, one could consider
# treating them e.g. by setting to 0.
train.target.describe()
# %%
for column in ["category", "color", "fabric", "season"]:
    px.histogram(train, x=column, title=f"Distribution of {column}").show()
# %%
random_codes = (
    train["external_code"].drop_duplicates().sample(3, random_state=42).values
)

# Keep in mind that "day", "week", "month" and "year" are not real time steps,
# but derived from "release_date" (see the main paper for more details)
px.line(
    train.query("external_code in @random_codes"),
    x="time_step",
    y="target",
    color="external_code",
    title="Sales of 3 random products",
)
# %%
train.groupby("external_code").target.describe()
# %% [markdown]
# ## Forecasting
# ### GTM Model
# %%
# Load forecasts from the GTM Model
gtm_forecasts = (
    pd.read_parquet(Path("data") / "VISUELLE_gtm-transformer.parquet")
    .rename(columns={"code": "external_code"})
    .query("external_code in @test.external_code")
    .assign(target=lambda x: x.target.round(0).astype(int))
)
# %%
models = (
    test.merge(
        gtm_forecasts,
        on=["time_step", "external_code", "target"],
        how="left",
    )
    .drop(columns="rmse")
    .rename(columns={"prediction": "GTM"})
)

assert len(models) == len(test)
assert len(models) == len(gtm_forecasts)
assert models.isna().any().any() == False
# %% [markdown]
# ### Simple statistical models
# %%
# Overall mean
models["OverallMean"] = train.target.mean()
# %%
# Mean sales in category
mean_const_map = (
    train.groupby(["category"])
    .target.mean()
    .reset_index()
    .rename(columns={"target": "MeanCategory"})
)
models = models.merge(mean_const_map, on="category", how="left")

# %%
# Mean time series of products of the same category
mean_ts_map = (
    train.groupby(["time_step", "category"])
    .target.mean()
    .reset_index()
    .rename(columns={"target": "MeanTSCategory"})
)

models = models.merge(mean_ts_map, on=["time_step", "category"], how="left")
# %%
px.line(
    mean_ts_map,
    x="time_step",
    y="MeanTSCategory",
    color="category",
    title="Mean sales of products of the same category per time step",
)
# %% [markdown]
# ### XGBoost
# Official documentation is available [here](https://xgboost.readthedocs.io/en/stable/).
# %%
cat_features = ["time_step", "color", "category", "fabric"]
num_features = ["year", "month", "day"]
features = cat_features + num_features


X_train = train[features].astype({col: "category" for col in cat_features})
y_train = train["target"]

X_test = test[features].astype({col: "category" for col in cat_features})
# %%
# TODO: tune hyperparameters to achieve better results
xgboost = xgb.XGBRegressor(tree_method="hist", max_depth=4, enable_categorical=True)
xgboost.fit(X_train, y_train)
xgboost_preds = xgboost.predict(X_test)
assert len(xgboost_preds) == len(models)
models["XGBoost"] = xgboost_preds
# %% [markdown]
# ### CatBoost
# > [CatBoost](https://catboost.ai/en/docs/) is a machine learning algorithm that uses gradient boosting on decision trees.
#
# You can find a tutorial on CatBoost [here](https://github.com/catboost/tutorials/blob/master/python_tutorial.ipynb).
# %%
features = ["time_step", "color", "category", "fabric"]
X_train = train[features].astype({col: "category" for col in features})
y_train = train["target"]

X_test = test[features].astype({col: "category" for col in features})
# %%
# TODO: tune hyperparameters to achieve better results
catboost = CatBoostRegressor(
    cat_features=features,
    verbose=False,
)
# %%
catboost.fit(X_train, y_train)
# %%
catboost_preds = catboost.predict(X_test)
assert len(catboost_preds) == len(models)
# %%
models["CatBoost"] = catboost_preds

# %% [markdown]
# ## Models Evaluation
# %%
model_names = [
    "OverallMean",
    "MeanTSCategory",
    "MeanCategory",
    "GTM",
    "XGBoost",
    "CatBoost",
]
# %%
# Show forecasts of the models for random products
random_items = test[["external_code", "category", "color"]].drop_duplicates().sample(3)

fig = make_subplots(
    rows=len(random_items),
    cols=1,
    shared_xaxes=True,
    subplot_titles=["/ ".join(list(item)) for item in random_items.astype(str).values],
    x_title="Time step",
    y_title="Sales",
    vertical_spacing=0.1,
)

for i, code in enumerate(random_items.external_code.values):
    for j, model in enumerate(model_names):
        fig.add_trace(
            go.Scatter(
                x=models.query("external_code == @code")["time_step"],
                y=models.query("external_code == @code")[model],
                name=model,
                mode="lines",
                legendgroup=model,
                showlegend=(i == 0),
            ),
            row=i + 1,
            col=1,
        )
    fig.add_trace(
        go.Scatter(
            x=models.query("external_code == @code")["time_step"],
            y=models.query("external_code == @code")["target"],
            name="Target",
            fill="tozeroy",
            mode="none",
            fillcolor="rgba(100,100,100,0.2)",
            legendgroup="Target",
            showlegend=(i == 0),
        ),
        row=i + 1,
        col=1,
    )

fig.update_layout(
    height=600,
    margin=dict(l=70, r=0, b=50, t=50),
)
fig.show()

# %%
metrics_dict = {
    "MAE": mean_absolute_error,
    "MSE": mean_squared_error,
    "RMSE": root_mean_squared_error,
}

# choose metric
metric = "RMSE"

metric_func = metrics_dict[metric]

# Stratified by meta column
meta_col = "time_step"
metric_stratified = models.groupby(meta_col).apply(
    lambda x: pd.Series(
        {model: metric_func(x["target"], x[model]) for model in model_names}
    )
)

fig = px.bar(
    metric_stratified.reset_index().melt(id_vars=meta_col),
    x=meta_col,
    y="value",
    color="variable",
    barmode="group",
    title=f"{metric} stratified by {meta_col}",
)
fig.update_layout(
    xaxis_title=meta_col,
    yaxis_title=metric,
)
# %%
# Overall
metric_overall = pd.Series(
    {model: metric_func(models["target"], models[model]) for model in model_names}
).sort_values()
print(f"Metric {metric} values:", metric_overall.round(1), sep="\n")
# %%
bias_overall = pd.Series(
    {model: (models[model] - models["target"]).mean() for model in model_names}
)
print(f"Bias values:", bias_overall.round(1), sep="\n")

# %%
fig = px.bar(
    metric_overall.reset_index().melt(id_vars="index"),
    x="index",
    y="value",
    title=f"{metric} overall per model",
)
fig.update_layout(
    xaxis_title="",
    yaxis_title=metric,
)
fig.show()
# %%
# Per category (we take only the largest 5 categories explicitly, the rest are grouped into "other")
items_per_category = (
    models.groupby("category")["external_code"].nunique().sort_values(ascending=False)
)
largest_5_categories = items_per_category.head(5).index
metric_per_category = (
    models.assign(
        category=lambda x: np.where(
            x["category"].isin(largest_5_categories), x["category"], "other"
        )
    )
    .groupby("category")
    .apply(
        lambda x: pd.Series(
            {model: metric_func(x["target"], x[model]) for model in model_names}
        )
    )
    .sort_index()
    .round(1)
)
metric_per_category
# %%
# Per calendar week
metric_per_week = (
    models.assign(week=lambda x: x["release_date"].dt.isocalendar().week)
    .groupby("week")
    .apply(
        lambda x: pd.Series(
            {model: metric_func(x["target"], x[model]) for model in model_names}
        )
    )
    .sort_index()
    .round(1)
)
metric_per_week
# %%

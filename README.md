# Cold Start Forecasting

This repository was created to support the  talk "Tackling the Cold Start Challenge in Demand Forecasting" presented in [PyData Berlin 2024 Talk](https://2024.pycon.de/program/H3X3AX/).

You can find all the references used in the presentation as well as Python code used to produce the presented experiments.

## References
1. [Hyndman & Athanasopoulos 2021 “Forecasting: principles and practice”](https://otexts.com/fpp3/)
2. [Montero-Manso, Hyndman 2021 "Principles and Algorithms for Forecasting Groups of Time Series: Locality and Globality"](https://arxiv.org/abs/2008.00444)
3. [Sukel et al 2023 "Multimodal Temporal Fusion Transformers Are Good Product Demand Forecasters"](https://arxiv.org/abs/2307.02578)
4. [Skenderi et al 2024 "Well googled is half done"](https://arxiv.org/abs/2109.09824)
5. [Kunz et al 2023 "Deep Learning based Forecasting: a case study from the online fashion industry"](https://arxiv.org/abs/2305.14406)
6. [Beretta et al 2016 "Nearest neighbor imputation algorithms: a critical evaluation"](https://bmcmedinformdecismak.biomedcentral.com/articles/10.1186/s12911-016-0318-z)
7. [Pavlyshenko 2019 "Machine-Learning Models for Sales Time Series Forecasting"](https://www.mdpi.com/2306-5729/4/1/15)
8. [Visuelle dataset](https://paperswithcode.com/dataset/visuelle)
9. [GTM-Transformer model repository](https://github.com/HumaticsLAB/GTM-Transformer) used in [4]

## Code
Stay tuned! 
The code to replicate the experiments will be added soon.

## Further Resources

### Peer Reviewed 
- [Chang, Lai 2005 "A hybrid system combining self-organizing maps with case-based reasoning in wholesaler's new-release book forecasting"](https://www.sciencedirect.com/science/article/abs/pii/S0957417405000187?via%3Dihub)
- [Chauhan et al 2020 "Time Series Forecasting for Cold-Start Items by Learning from Related Items using Memory Networks"](https://dl.acm.org/doi/abs/10.1145/3366424.3382728)
- [Moon et al 2020 "Solving the Cold-Start Problem in Short-Term Load Forecasting Using Tree-Based Methods"](https://www.mdpi.com/1996-1073/13/4/886)
- [Ekambaram et al 2020 "Attention based Multi-Modal New Product Sales Time-series Forecasting"](https://dl.acm.org/doi/abs/10.1145/3394486.3403362)

### Preprints
- [Fatemi et al 2023+ "Mitigating Cold-start Forecasting using Cold Causal Demand Forecasting Model"](https://arxiv.org/pdf/2306.09261)
- [Xie et al 2018 "A Unified Framework for Long Range and Cold Start Forecasting of Seasonal Profiles in Time Series"](https://arxiv.org/pdf/1710.08473)

### Other
- [Borne, Allen 2019 "Data Scientist’s Dilemma: The Cold Start Problem – Ten Machine Learning Examples"](https://www.kdnuggets.com/2019/01/data-scientist-dilemma-cold-start-machine-learning.html)
- [Godahewa et al 2021 "Monash Time Series Forecasting Repository"](https://forecastingdata.org/)
- [Toronto Machine Learning Series 2023 "Overcoming the Cold Start Problem: how to Make New Tasks Tractable"](https://www.youtube.com/watch?v=Ch33NYL3Seg)


### Authors
- Alexander Meier
- Daria Mokrytska
